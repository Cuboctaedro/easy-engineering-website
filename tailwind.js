
let defaultConfig = require('tailwindcss/defaultConfig')()


let colors = {
    'transparent'   : 'transparent',

    'black'         : '#000000',

    'grey-darkest'  : '#181B1E',
    'grey-darker'   : '#31383E',
    'grey-dark'     : '#4C545C',
    'grey'          : '#67717A',
    'grey-light'    : '#949CA3',
    'grey-lighter'  : '#C1C6CB',
    'grey-lightest' : '#F0F1F2',

    'white'         : '#ffffff',

    'pink-darkest'  : '#3A0E24',
    'pink-darker'   : '#73224B',
    'pink-dark'     : '#A63C71',
    'pink'          : '#D45A97',
    'pink-light'    : '#E480B2',
    'pink-lighter'  : '#F1A9CD',
    'pink-lightest' : '#FAD6E8',

    'blue-darkest'  : '#083339',
    'blue-darker'   : '#156470',
    'blue-dark'     : '#2991A0',
    'blue'          : '#42bacb',
    'blue-light'    : '#6ED0DE',
    'blue-lighter'  : '#9DE3ED',
    'blue-lightest' : '#D0F4F9',

    'lime-darkest'  : '#373100',
    'lime-darker'   : '#6B6004',
    'lime-dark'     : '#99890F',
    'lime'          : '#BFAD22',
    'lime-light'    : '#D6C755',
    'lime-lighter'  : '#E9DE8D',
    'lime-lightest' : '#F7F1C9',


}

module.exports = {

    colors: colors,


    screens: {
        'sm': '480px',
        'md': '768px',
        'lg': '1280px',
      },



    fonts: {
        'sans': [
            'Roboto',
            'Helvetica Neue',
            'sans-serif',
        ],
        'serif': [
            'Georgia Pro',
            'Georgia',
            'serif',
        ],
        'mono': [
            'Roboto Mono',
            'Consolas',
            'monaco',
            'monospace',
        ],
    },

    textSizes: {
        '12' : '.75rem',
        '14' : '.875rem',
        '16' : '1rem',
        '18' : '1.125rem',
        '24' : '1.5rem',
        '30' : '1.875rem',
        '36' : '2.25rem',
        '48' : '3rem',
        '72' : '4.5rem',
    },


    fontWeights: {
        'hairline' : 100,
        'thin'     : 200,
        'light'    : 300,
        'regular'  : 400,
        'medium'   : 500,
        'semibold' : 600,
        'bold'     : 700,
        'extrabold': 800,
        'black'    : 900,
    },


    leading: {
        'none': 1,
        'tight': 1.25,
        'normal': 1.5,
        'loose': 2,
    },


    tracking: {
        'tight': '-0.05em',
        'normal': '0',
        'wide': '0.05em',
    },


    textColors: colors,


    backgroundColors: colors,


    backgroundSize: {
        'auto': 'auto',
        'cover': 'cover',
        'contain': 'contain',
    },


    borderWidths: {
        default: '1px',
        '0': '0',
        '2': '2px',
        '4': '4px',
        '8': '8px',
    },


    borderColors: global.Object.assign({ default: colors['grey-light'] }, colors),


    borderRadius: {
        'none': '0',
        'sm': '.125rem',
        default: '.25rem',
        'lg': '.5rem',
        'full': '9999px',
    },


    width: {
        'auto': 'auto',
        'px'  : '1px',
        '0'   : '0',
        '1'   : '0.5rem',
        '2'   : '1rem',
        '3'   : '1.5rem',
        '4'   : '2rem',
        '5'   : '2.5rem',
        '6'   : '3rem',
        '8'   : '4rem',
        '9'   : '4.5rem',
        '10'  : '5rem',
        '12'  : '6rem',
        '15'  : '7.5rem',
        '16'  : '8rem',
        '18'  : '9rem',
        '24'  : '12rem',
        '32'  : '16rem',
        '48'  : '24rem',
        '64'  : '32rem',
        '96'  : '48rem',
        '120' : '60rem',
        '1/2' : '50%',
        '1/3' : '33.33333%',
        '2/3' : '66.66667%',
        '1/4' : '25%',
        '3/4' : '75%',
        '1/5' : '20%',
        '2/5' : '40%',
        '3/5' : '60%',
        '4/5' : '80%',
        '1/6' : '16.66667%',
        '5/6' : '83.33333%',
        'full': '100%',
        'screen': '100vw',
    },


    height: {
        'auto'  : 'auto',
        'px'  : '1px',
        '0'   : '0',
        '1'   : '0.5rem',
        '2'   : '1rem',
        '3'   : '1.5rem',
        '4'   : '2rem',
        '5'   : '2.5rem',
        '6'   : '3rem',
        '8'   : '4rem',
        '9'   : '4.5rem',
        '10'  : '5rem',
        '12'  : '6rem',
        '15'  : '7.5rem',
        '16'  : '8rem',
        '18'  : '9rem',
        '24'  : '12rem',
        '32'    : '16rem',
        '48'    : '24rem',
        'full'  : '100%',
        'screen': '100vh',
    },


    minWidth: {
        'px'  : '1px',
        '0'   : '0',
        '1'   : '0.5rem',
        '2'   : '1rem',
        '3'   : '1.5rem',
        '4'   : '2rem',
        '5'   : '2.5rem',
        '6'   : '3rem',
        '8'   : '4rem',
        '9'   : '4.5rem',
        '10'  : '5rem',
        '12'  : '6rem',
        '15'  : '7.5rem',
        '16'  : '8rem',
        '18'  : '9rem',
        '24'  : '12rem',
        '32'  : '16rem',
        '48'  : '24rem',
        '64'  : '32rem',
        '96'  : '48rem',
        'full': '100%',
    },


    minHeight: {
        '0': '0',
        '1'   : '0.5rem',
        '2'   : '1rem',
        '3'   : '1.5rem',
        '4'   : '2rem',
        '5'   : '2.5rem',
        '6'   : '3rem',
        '8'   : '4rem',
        '9'   : '4.5rem',
        '10'  : '5rem',
        '12'  : '6rem',
        '15'  : '7.5rem',
        '16'  : '8rem',
        '18'  : '9rem',
        '24'  : '12rem',
        '32'  : '16rem',
        '48'  : '24rem',
        '64'  : '32rem',
        '96'  : '48rem',
        'full': '100%',
        'screen': '100vh',
    },


    maxWidth: {
        'px'  : '1px',
        '0'   : '0',
        '1'   : '0.5rem',
        '2'   : '1rem',
        '3'   : '1.5rem',
        '4'   : '2rem',
        '5'   : '2.5rem',
        '6'   : '3rem',
        '8'   : '4rem',
        '9'   : '4.5rem',
        '10'  : '5rem',
        '12'  : '6rem',
        '15'  : '7.5rem',
        '16'  : '8rem',
        '18'  : '9rem',
        '24'  : '12rem',
        '32'  : '16rem',
        '48'  : '24rem',
        '64'  : '32rem',
        '96'  : '48rem',
        'full': '100%',
    },


    maxHeight: {
        '0': '0',
        'full': '100%',
        'screen': '100vh',
    },


    padding: {
        'px'  : '1px',
        '0'   : '0',
        '1'   : '0.5rem',
        '2'   : '1rem',
        '3'   : '1.5rem',
        '4'   : '2rem',
        '5'   : '2.5rem',
        '6'   : '3rem',
        '8'   : '4rem',
        '9'   : '4.5rem',
        '10'  : '5rem',
        '12'  : '6rem',
        '15'  : '7.5rem',
        '16'  : '8rem',
        '18'  : '9rem',
        '24'  : '12rem',
    },


    margin: {
        'auto': 'auto',
        'px'  : '1px',
        '0'   : '0',
        '1'   : '0.5rem',
        '2'   : '1rem',
        '3'   : '1.5rem',
        '4'   : '2rem',
        '5'   : '2.5rem',
        '6'   : '3rem',
        '8'   : '4rem',
        '9'   : '4.5rem',
        '10'  : '5rem',
        '12'  : '6rem',
        '15'  : '7.5rem',
        '16'  : '8rem',
        '18'  : '9rem',
        '24'  : '12rem',
    },

    negativeMargin: {
        'auto': 'auto',
        'px'  : '1px',
        '0'   : '0',
        '1'   : '0.5rem',
        '2'   : '1rem',
        '3'   : '1.5rem',
        '4'   : '2rem',
        '5'   : '2.5rem',
        '6'   : '3rem',
        '8'   : '4rem',
        '9'   : '4.5rem',
        '10'  : '5rem',
        '12'  : '6rem',
        '15'  : '7.5rem',
        '16'  : '8rem',
        '18'  : '9rem',
        '24'  : '12rem',
    },


    shadows: {
        '1'   : '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
        '2'   : '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
        '3'   : '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)',
        '4'   : '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)',
        '5'   : '0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)',
        'none': 'none',
    },


    zIndex: {
        'auto': 'auto',
        '-20': -20,
        '-10': -10,
        '0': 0,
        '10': 10,
        '20': 20,
        '30': 30,
        '40': 40,
        '50': 50,
    },


    opacity: {
        '0': '0',
        '25': '.25',
        '50': '.5',
        '75': '.75',
        '100': '1',
    },


    svgFill: {
        'current': 'currentColor',
    },


    svgStroke: {
        'current': 'currentColor',
    },


    modules: {
        appearance: ['responsive'],
        backgroundAttachment: ['responsive'],
        backgroundColors: ['responsive', 'hover', 'focus'],
        backgroundPosition: ['responsive'],
        backgroundRepeat: ['responsive'],
        backgroundSize: ['responsive'],
        borderCollapse: [],
        borderColors: ['hover', 'focus'],
        borderRadius: ['responsive'],
        borderStyle: ['responsive'],
        borderWidths: ['responsive'],
        cursor: [],
        display: ['responsive'],
        flexbox: ['responsive'],
        float: ['responsive'],
        fonts: ['responsive'],
        fontWeights: ['responsive'],
        height: ['responsive'],
        leading: ['responsive'],
        lists: ['responsive'],
        margin: ['responsive'],
        maxHeight: ['responsive'],
        maxWidth: ['responsive'],
        minHeight: ['responsive'],
        minWidth: ['responsive'],
        negativeMargin: ['responsive'],
        objectFit: false,
        objectPosition: false,
        opacity: ['responsive', 'hover', 'focus'],
        outline: ['focus'],
        overflow: ['responsive'],
        padding: ['responsive'],
        pointerEvents: ['responsive'],
        position: ['responsive'],
        resize: ['responsive'],
        shadows: ['responsive', 'hover', 'focus'],
        svgFill: [],
        svgStroke: [],
        tableLayout: ['responsive'],
        textAlign: ['responsive'],
        textColors: ['responsive', 'hover', 'focus'],
        textSizes: ['responsive'],
        textStyle: ['responsive'],
        tracking: ['responsive'],
        userSelect: ['responsive'],
        verticalAlign: ['responsive'],
        visibility: ['responsive'],
        whitespace: ['responsive'],
        width: ['responsive'],
        zIndex: ['responsive'],
    },


    plugins: [
        require('tailwindcss/plugins/container')({
        }),
    ],


    options: {
        prefix: '',
        important: false,
        separator: ':',
    },

}
