

document.querySelectorAll('[data-dropdown-opens]').forEach(function(el) {
    var menu = document.querySelectorAll(el.dataset.dropdownOpens)[0];
    el.addEventListener("click", function() {
        var expanded = el.getAttribute('aria-expanded') === 'true' || false;
        el.setAttribute('aria-expanded', !expanded);
        menu.hidden = !menu.hidden;
    }, false);

})
